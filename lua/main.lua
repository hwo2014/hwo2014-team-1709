socket = require("socket")
json = require("json")
logger = require("logger")
hwo2014bot = require("hwo2014bot")
simulation = require("simulation")
engine = require("engine")
connection = require("connection")
data = require("data")

race_protocol = "ci"

if race_protocol == "ci" then
	hwo2014bot.qualifying = true
else
	hwo2014bot.qualifying = false
end

local callbacks = {}
local function control_loop()
	local host, port, name, key = unpack(arg)
	connection.host = host
	connection.port = port
	connection.name = name
	connection.key = key
	connection.connect()
	
	if race_protocol == "ci" then
		connection.join()
	else
		local n_cars = 4
		local password = "sadfa42ga43"
		local track = "imola"
		connection.join_race(track, password, n_cars)
	end
	
	--server message driven control loop
	while true do
		local msg = connection.read_msg()
		if msg == nil then break end
		
		local msg_type = msg.msgType
		if msg_type ~= "carPositions" then logger.write(msg_type) end
		local callback = callbacks[msg_type]
		if callback then
			callback(msg)
		else
			logger.write("unhandled message type: " .. msg_type)
		end
	end
end

callbacks.gameInit = function(msg)
	data.gameInit = msg
	data.add_true_radiuses()
	data.add_lane_lengths()
end

callbacks.join = function(msg)
end

callbacks.yourCar = function(msg)
	data.yourCar = msg
end

callbacks.carPositions = function(msg)
	connection.tick_received()

	data.game_tick = msg.gameTick
	data.carPositions[#data.carPositions + 1] = msg
	data.add_own_car_position()
	
	-- first car positions message is different
	if not msg.gameTick then
		connection.sync.ticks_received = 0
		connection.sync.ticks_sent = 0
		data.carPositions[#data.carPositions].own_data.speed = 0
	else
		connection.sync.ticks_received = connection.sync.ticks_received + 1
		data.add_car_speeds()
		hwo2014bot.tick()
	end
end

callbacks.createRace = function(msg)
	data.createRace = msg
	logger.write("track: " .. msg.data.trackName)
end


callbacks.gameStart = function(msg)
	data.game_tick = msg.gameTick
	
	-- evaluate tick and send response
	hwo2014bot.tick()
end

callbacks.lapFinished = function(msg)
end

callbacks.gameEnd = function(msg)
	-- change race stage on ci server
	if hwo2014bot.qualifying then hwo2014bot.qualifying = false end

	-- flush tick data table
	data.carPositions = {}
end

callbacks.tournamentEnd = function(msg)
end

callbacks.finish = function(msg)
end

callbacks.crash = function(msg)
	if msg.data.name == data.yourCar.data.name then
		-- crash wipes turbo
		if data.turboAvailable then data.turboAvailable = nil end
		if data.turboAvailable_bank then data.turboAvailable_bank = nil end
		
		-- reset turbo usage records on crash
		simulation.turbo.turbos_left_unused = 0 
	end
end

callbacks.spawn = function(msg)
	if msg.data.name == data.yourCar.data.name then
		-- spawn wipes turbo
		if data.turboAvailable then data.turboAvailable = nil end
		if data.turboAvailable_bank then data.turboAvailable_bank = nil end
		
		-- reset turbo usage records on spawn
		simulation.turbo.turbos_left_unused = 0 
	end
end

callbacks.turboAvailable = function(msg)
	-- dont overwrite old turbo info if its activated
	if data.turboAvailable and data.turboAvailable.start then
		data.turboAvailable_bank = msg
	else
		data.turboAvailable = msg
	end

	-- keep track of turbo interval in race stage
	if not hwo2014bot.qualifying and not simulation.turbo.interval then
		simulation.turbo.interval = msg.gameTick
	end
	
	-- lower turboed full throttle tick expectations if turbos are not being used
	if simulation.turbo.turbos_left_unused > 0 then
		simulation.turbo.throttle_depth_trigger = simulation.turbo.throttle_depth_trigger - simulation.turbo.turbos_left_unused
		if simulation.turbo.throttle_depth_trigger < 10 then simulation.turbo.throttle_depth_trigger = 10 end
		logger.write("turbo trigger lowered: " .. simulation.turbo.throttle_depth_trigger)
	end
	simulation.turbo.turbos_left_unused = simulation.turbo.turbos_left_unused + 1

	logger.write("turbo available " .. msg.gameTick)
end

callbacks.turboStart = function(msg)
	if msg.data.name == data.yourCar.data.name then
		data.turboAvailable.start = msg.gameTick
		logger.write("turbo start with trigger: " .. simulation.turbo.throttle_depth_trigger)
		
		-- reset turbo usage records on succesfull turbo usage
		simulation.turbo.turbos_left_unused = 0 
	end
end

callbacks.turboEnd = function(msg)
	if msg.data.name == data.yourCar.data.name then
		data.turboAvailable.deferred_deactivation = true
		logger.write("turbo end")
	end
end

assert(#arg == 4)
control_loop()