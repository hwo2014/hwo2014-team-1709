local data = {}
data.carPositions = {}

data.sign = function(v)
	if v < 0 then return -1 else return 1 end
end

data.add_own_car_position = function()
	for i = 1, #data.gameInit.data.race.cars do
		if data.carPositions[#data.carPositions].data[i].id.name == data.yourCar.data.name then
			data.carPositions[#data.carPositions].own_data = data.carPositions[#data.carPositions].data[i]
			return
		end
	end
end

data.own_car_index = function()
	for i = 1, #data.gameInit.data.race.cars do
		if data.carPositions[#data.carPositions].data[i].id.name == data.yourCar.data.name then
			return i
		end
	end
end

data.add_true_radiuses = function()
	local pieces = data.gameInit.data.race.track.pieces
	local lanes = data.gameInit.data.race.track.lanes
	for piece = 1, #pieces do
		if not pieces[piece].length then
			pieces[piece].true_radiuses = {}
			for lane = 1, #lanes do
				local true_radius = math.abs(data.sign(pieces[piece].angle) * pieces[piece].radius - lanes[lane].distanceFromCenter)
				pieces[piece].true_radiuses[lane] = true_radius
			end
		end
	end
end

data.add_lane_lengths = function()
	local pieces = data.gameInit.data.race.track.pieces
	local lanes = data.gameInit.data.race.track.lanes
	for piece = 1, #pieces do
		if not pieces[piece].length then
			pieces[piece].lengths = {}
			for lane = 1, #lanes do
				local true_radius = pieces[piece].true_radiuses[lane]
				local lane_length = (math.abs(pieces[piece].angle) / 360) * 2 * math.pi * true_radius
				pieces[piece].lengths[lane] = lane_length
			end
		end
	end
end

function data.add_car_speeds()
	local hp = engine.race_parameters.hp
	if data.turboAvailable and data.turboAvailable.start then
		hp = hp * data.turboAvailable.data.turboFactor
	end
	local rr = engine.race_parameters.rr
	local own_data = data.carPositions[#data.carPositions - 1].own_data
	
	for car = 1, #data.carPositions[#data.carPositions].data do
		local current_index = data.carPositions[#data.carPositions].data[car].piecePosition.pieceIndex + 1
		local previous_index = data.carPositions[#data.carPositions - 1].data[car].piecePosition.pieceIndex + 1
		
		
		-- if piece index doesnt change or first few ticks of the race
		if current_index == previous_index or #data.carPositions <= 3 then
			local current_distance = data.carPositions[#data.carPositions].data[car].piecePosition.inPieceDistance
			local previous_distance = data.carPositions[#data.carPositions - 1].data[car].piecePosition.inPieceDistance
			data.carPositions[#data.carPositions].data[car].speed = current_distance - previous_distance
	

		-- if piece index changed without having constants to calculate real distance
		elseif not hp or not rr then
			local lane = data.carPositions[#data.carPositions].data[car].piecePosition.lane.endLaneIndex + 1
			local previous_piece_length = data.piece_length(previous_index, lane)
			
			local current_distance = data.carPositions[#data.carPositions].data[car].piecePosition.inPieceDistance
			local previous_distance = data.carPositions[#data.carPositions - 1].data[car].piecePosition.inPieceDistance
			data.carPositions[#data.carPositions].data[car].speed = previous_piece_length - previous_distance +  current_distance
	
		-- else calculate speed from throttle and hp, result might not be right for other cars
		else 
			local previous_speed = data.carPositions[#data.carPositions - 1].data[car].speed
			local throttle = data.carPositions[#data.carPositions - 1].throttle
			data.carPositions[#data.carPositions].data[car].speed = previous_speed + throttle * hp - previous_speed * rr
		end
	end
end

function data.piece_length(piece_index, lane)
	if data.gameInit.data.race.track.pieces[piece_index].length then
		return data.gameInit.data.race.track.pieces[piece_index].length
	else
		return data.gameInit.data.race.track.pieces[piece_index].lengths[lane]
	end
end

function data.distance_car_to_piece(car, piece)
	local car_pi = data.carPositions[#data.carPositions].data[car].piecePosition.pieceIndex + 1
	local car_pd = data.carPositions[#data.carPositions].data[car].piecePosition.inPieceDistance
	local car_lane = data.carPositions[#data.carPositions].data[car].piecePosition.lane.endLaneIndex + 1
	
	local distance = data.piece_length(car_pi, car_lane) - car_pd
	local piece_ite = data.next_piece_index(car_pi)
	while piece_ite ~= piece do
		distance = distance + data.piece_length(piece_ite, car_lane)
		piece_ite = data.next_piece_index(piece_ite)
	end
	return distance
end

function data.switch_piece_approaching()
	local speed = data.carPositions[#data.carPositions].own_data.speed
	local switch_distance_buffer = speed * 2.0
	
	local piece_index = data.carPositions[#data.carPositions].own_data.piecePosition.pieceIndex + 1
	local lane = data.carPositions[#data.carPositions].own_data.piecePosition.lane.endLaneIndex + 1
	
	-- distance left in current piece
	local distance_to_curve = data.piece_length(piece_index, lane) - data.carPositions[#data.carPositions].own_data.piecePosition.inPieceDistance
	
	-- sum of the piece lengths until next lane switch
	local piece_index_ite = data.next_piece_index(piece_index)
	while not data.gameInit.data.race.track.pieces[piece_index_ite].switch do
		distance_to_curve = distance_to_curve + data.piece_length(piece_index_ite, lane)
		piece_index_ite = data.next_piece_index(piece_index_ite)
	end

	if distance_to_curve < switch_distance_buffer then
		if distance_to_curve < speed then return false end
		return true
	else
		return false
	end
end

function data.car_between_piece_indices(car, start_index, end_index)
	local piece_index_ite = start_index
	while piece_index_ite ~= end_index do
		local car_piece_index = data.carPositions[#data.carPositions].data[car].piecePosition.pieceIndex + 1
		if car_piece_index == piece_index_ite then
			return true
		end
		piece_index_ite = data.next_piece_index(piece_index_ite)
	end
	return false
end

function data.shortest_lane_direction()
	local ite = data.carPositions[#data.carPositions].own_data.piecePosition.pieceIndex + 1
	--iterate to next switch
	ite = data.next_piece_index(ite)
	while not data.gameInit.data.race.track.pieces[ite].switch do
		ite = data.next_piece_index(ite)
	end
	local switch_start = ite
	
	--iterate to next switch
	ite = data.next_piece_index(ite)
	while not data.gameInit.data.race.track.pieces[ite].switch do
		ite = data.next_piece_index(ite)
	end
	local switch_end = ite
	
	-- sum angles between switches
	local sum_angle = 0
	for i = switch_start, switch_end do
		if data.gameInit.data.race.track.pieces[i].angle then
			sum_angle = sum_angle + data.gameInit.data.race.track.pieces[i].angle
		end
	end
	
	local lane = data.carPositions[#data.carPositions].own_data.piecePosition.lane.endLaneIndex + 1
	if sum_angle < 0 then
		if lane ~= 1 then
			return "Left"
		end
	elseif sum_angle > 0 then
		if lane ~= #data.gameInit.data.race.track.lanes then
			return "Right"
		end
	end
	return nil
end

function data.free_lane_direction()
	local lane = data.carPositions[#data.carPositions].own_data.piecePosition.lane.endLaneIndex + 1
	if lane ~= 1 then
		return "Left"
	elseif lane ~= #data.gameInit.data.race.track.lanes then
		return "Right"
	end
	return nil
end

data.next_piece_index = function(index)
	local next_index = index + 1
	if next_index > #data.gameInit.data.race.track.pieces then
		return 1
	else
		return next_index
	end
end

return data