local hwo2014bot = {}

function hwo2014bot.tick()
	engine.determine_constants()
	simulation.generate_race_output()

	hwo2014bot.deferred_turbo_deactivation()
	logger.status()
end

function hwo2014bot.deferred_turbo_deactivation()
	-- turbo is deactivated with flag to do it one tick 'late'
	if data.turboAvailable and data.turboAvailable.deferred_deactivation then
		data.turboAvailable = nil
		if data.turboAvailable_bank then
			data.turboAvailable = data.turboAvailable_bank
			data.turboAvailable_bank = nil
		end
	end
end

return hwo2014bot