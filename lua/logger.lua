local logger = {}

local DEBUG = false

function logger.write(str)
	if DEBUG and str then
		if not logger.file then
			logger.file = assert(io.open(connection.name .. ".txt", "w"))
		end
		logger.file:write(str .. "\n")
		print(str)
	end
end

function logger.status()
	if not DEBUG then return end

	if #data.carPositions < 3 then return end
	
	local pieceIndex = data.carPositions[#data.carPositions].own_data.piecePosition.pieceIndex + 1
	local laneIndex = data.carPositions[#data.carPositions].own_data.piecePosition.lane.endLaneIndex + 1
	local speed = data.carPositions[#data.carPositions].own_data.speed
	local radius = 0
	local curve_sign = 0
	if data.gameInit.data.race.track.pieces[pieceIndex].angle then
		radius = data.gameInit.data.race.track.pieces[pieceIndex].true_radiuses[laneIndex]
		curve_sign = data.sign(data.gameInit.data.race.track.pieces[pieceIndex].angle)
	end
	
	local angle = data.carPositions[#data.carPositions].own_data.angle
	local angle_speed = angle - data.carPositions[#data.carPositions - 1].own_data.angle
	local angle_acceleration = angle_speed - (data.carPositions[#data.carPositions - 1].own_data.angle - data.carPositions[#data.carPositions - 2].own_data.angle)
	
	local pi = data.carPositions[#data.carPositions].own_data.piecePosition.pieceIndex + 1
	local pd = data.carPositions[#data.carPositions].own_data.piecePosition.inPieceDistance
	
	local sl = data.carPositions[#data.carPositions].own_data.piecePosition.lane.startLaneIndex + 1
	local el = data.carPositions[#data.carPositions].own_data.piecePosition.lane.endLaneIndex + 1
	
	local throttle = data.carPositions[#data.carPositions].throttle
	
	logger.write(pi .. "\t" .. sl .. "\t" .. el .. "\t" .. pd .. "\t" .. curve_sign .. "\t" .. radius .. "\t" .. speed .. "\t" .. angle .. "\t" .. angle_speed .. "\t" .. angle_acceleration .. "\t" .. throttle)
end

return logger