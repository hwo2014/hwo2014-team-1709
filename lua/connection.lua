local connection = {}

connection.in_sync = true

connection.timers = {}
connection.timers.response_time = 0
connection.timers.time_received = 0
connection.timers.time_responded = 0

connection.sync = {}
connection.sync.ticks_received = 0
connection.sync.ticks_sent = 0

function connection.connect()
	connection.connection = assert(socket.connect(connection.host, connection.port))
end

function connection.tick_received()
	connection.timers.time_received = socket.gettime()
end

function connection.response_sent()
	connection.timers.time_responded = socket.gettime()
	if (connection.timers.time_responded - connection.timers.time_received) > (1 / 60) then
		logger.write(string.format("too long response delay: %f", connection.timers.time_responded - connection.timers.time_received))
	end
end

function connection.send(msgtype, msg, tick)
	if tick then
		connection.response_sent()
		if data.game_tick ~= connection.sync.ticks_sent then
			connection.in_sync = false
			connection.sync.ticks_sent = data.game_tick + 1
			logger.write("out of sync")
		end
		connection.sync.ticks_sent = connection.sync.ticks_sent + 1
	end

	local line
	if tick and connection.in_sync then
		line = connection.json_msg_tick(msgtype, msg, tick)
	else
		line = connection.json_msg(msgtype, msg)
	end
	connection.connection:send(line .. "\n")
end

function connection.json_msg(msgtype, data)
	local msg = json.encode.encode({msgType = msgtype, data = data})
	--logger.write(string.format("client: %s", msg))
	return msg
end

function connection.json_msg_tick(msgtype, data, tick)
	local msg = json.encode.encode({msgType = msgtype, data = data, gameTick = tick})
	--logger.write(string.format("client: %s", msg))
	return msg
end

function connection.read_msg()
	local line = connection.connection:receive("*l")
	if line then
		--logger.write(string.format("server: %s", line))
		return json.decode.decode(line)
	end
end

function connection.join()
	connection.send("join", {name = connection.name, key = connection.key})
end

function connection.create_race(track, pw, n_cars)
	connection.send("createRace", {botId = {name = connection.name, key = connection.key}, trackName = track, password = pw, carCount = n_cars})
end

function connection.join_race(track, pw, n_cars)
	connection.send("joinRace", {botId = {name = connection.name, key = connection.key}, trackName = track, password = pw, carCount = n_cars})
end

function connection.throttle(throttle)
	-- record sent throttle
	data.carPositions[#data.carPositions].throttle = throttle
	connection.send("throttle", throttle, data.game_tick)
end

function connection.throttle_no_gametick(throttle)
	-- record sent throttle
	data.carPositions[#data.carPositions].throttle = throttle
	connection.send("throttle", throttle)
end

function connection.switch_lane(direction)
	logger.write("sent: " .. direction)
	
	-- record sent switch
	simulation.switch.locked_switch_direction = direction
	-- iterate to the piece that the switch message is tight to
	local ite = data.carPositions[#data.carPositions].own_data.piecePosition.pieceIndex + 1
	local ite = data.next_piece_index(ite)
	while not data.gameInit.data.race.track.pieces[ite].switch do
		ite = data.next_piece_index(ite)
	end
	simulation.switch.locked_switch_index = ite
	
	-- add throttle from previous tick
	data.carPositions[#data.carPositions].throttle = data.carPositions[#data.carPositions - 1].throttle
	
	connection.send("switchLane", direction, data.game_tick)
end

function connection.turbo()
	logger.write("sent: Turbo")
	
	-- add throttle from previous tick
	data.carPositions[#data.carPositions].throttle = data.carPositions[#data.carPositions - 1].throttle
	connection.send("turbo", "Must kill Moe... Weeeee!", data.game_tick)
end

return connection