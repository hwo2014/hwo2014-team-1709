local simulation = {}

simulation.search_depth = 80
simulation.binary_search_depth = 0

simulation.race_parameters = {}
simulation.race_parameters.angle_limit = 60.0


simulation.turbo = {}
simulation.turbo.verification = nil
simulation.turbo.throttle_depth_trigger = 17
simulation.turbo.turbos_left_unused = 0


simulation.switch = {}
simulation.switch.locked_switch_direction = nil
simulation.switch.locked_switch_index = nil
simulation.switch.presumed_best = nil

function simulation.generate_race_output()
	if simulation.unknown_constants_behaviour() then
		return
	end
	
	-- clear locked switch when past it
	local current_index = data.carPositions[#data.carPositions].own_data.piecePosition.pieceIndex + 1
	local previous_index = data.carPositions[#data.carPositions - 1].own_data.piecePosition.pieceIndex + 1
	if previous_index ~= current_index then
		if simulation.switch.locked_switch_index and simulation.switch.locked_switch_index == previous_index then
			simulation.switch.locked_switch_direction = nil
			simulation.switch.locked_switch_index = nil
		end
	end

	-- execute locked lane switch
	if simulation.switch.locked_switch_index then
		if simulation.turbo_tester(simulation.switch.locked_switch_direction) then
			connection.turbo()
			return
		end
	
		if simulation.simulate(simulation.search_depth, 1.0, simulation.switch.locked_switch_direction) then
			connection.throttle(1.0)
			return
		else
			connection.throttle(0.0)
			return
		end
	end
	
	-- check lane switch choises
	if data.switch_piece_approaching() then
		if simulation.switch_safe() then
			if simulation.between_switches_obstacle_check() then
				simulation.switch.presumed_best = data.free_lane_direction()
			else
				simulation.switch.presumed_best = data.shortest_lane_direction()
			end
		else
			simulation.switch.presumed_best = nil
		end
		
		if simulation.switch.presumed_best then
			-- tested with the last ticks throttle
			if simulation.simulate(simulation.search_depth, data.carPositions[#data.carPositions - 1].throttle, simulation.switch.presumed_best) then
				connection.switch_lane(simulation.switch.presumed_best)
				simulation.switch.presumed_best = nil
				return
			end
		end
	end
	
	-- simulate best assumed lane path
	if current_index ~= previous_index then
		simulation.switch.presumed_best = data.shortest_lane_direction()
	end
	
	if simulation.turbo_tester(simulation.switch.presumed_best) then
		connection.turbo()
		return
	end
	if simulation.simulate(simulation.search_depth, 1.0, simulation.switch.presumed_best) then
		connection.throttle(1.0)
		return
	else
		connection.throttle(0.0)
		return
	end
end

function simulation.unknown_constants_behaviour()
	if #data.carPositions <= 3 then
		connection.throttle(1.0)
		return true
	end
	
	-- no lane switching until friction is known
	if not engine.race_parameters.friction then
		if simulation.simulate(simulation.search_depth, 1.0) then
			connection.throttle(1.0)
			return true
		else
			connection.throttle(0.0)
			return true
		end
	end
	return false
end

function simulation.switch_safe()
	local current_index = data.carPositions[#data.carPositions].own_data.piecePosition.pieceIndex + 1
	local switch_index = data.next_piece_index(current_index)
	while not data.gameInit.data.race.track.pieces[switch_index].switch do
		switch_index = data.next_piece_index(switch_index)
	end

	local cars = data.carPositions[#data.carPositions].data
	local own_car = data.own_car_index()
	local own_lane = data.carPositions[#data.carPositions].own_data.piecePosition.lane.endLaneIndex
	for car = 1, #cars do
		if car ~= own_car then
			if own_lane ~= cars[car].piecePosition.lane.endLaneIndex then
				if data.car_between_piece_indices(car, current_index, next_index) then
					local dist_own_to_switch = data.distance_car_to_piece(own_car, switch_index)
					local dist_obs_to_switch = data.distance_car_to_piece(car, switch_index)
					local own_speed = cars[own_car].speed + (1/1000000) -- zero speed not valid
					local obs_speed = cars[car].speed + (1/1000000) -- zero speed not valid
					-- move the cars forward at their current pace until our car reaches the switch
					for i = 1, 100 do
						if dist_own_to_switch < 0 then break end
						dist_own_to_switch = dist_own_to_switch - own_speed
						dist_obs_to_switch = dist_obs_to_switch - obs_speed
					end

					local car_spacing = dist_own_to_switch - dist_obs_to_switch
					local car_length = 40
					local error_margin = 1.1
					-- if probable collision
					if math.abs(car_spacing) < car_length * error_margin then
						-- if we are ahead
						if car_spacing - 4 < 0 then
							-- if speeds almost match and projected angle is high enough do not switch
							if math.abs(own_speed - obs_speed) < 0.03 * own_speed then
								local _, angle = simulation.simulate(simulation.search_depth, 1.0)
								if angle > 50 then return false end
							end
						end
					end
				end
			end
		end
	end
	return true
end

function simulation.between_switches_obstacle_check()
	local current_index = data.carPositions[#data.carPositions].own_data.piecePosition.pieceIndex + 1

	-- starting switch piece
	local start_switch_piece = data.next_piece_index(current_index)
	while not data.gameInit.data.race.track.pieces[start_switch_piece].switch do
		start_switch_piece = data.next_piece_index(start_switch_piece)
	end
	
	-- ending piece of obstacle check
	local end_switch_piece = data.next_piece_index(start_switch_piece)
	while not data.gameInit.data.race.track.pieces[end_switch_piece].switch do
		end_switch_piece = data.next_piece_index(end_switch_piece)
	end
	
	local cars = data.carPositions[#data.carPositions].data
	local own_car = data.own_car_index()
	local own_lane = data.carPositions[#data.carPositions].own_data.piecePosition.lane.endLaneIndex
	
	local primary_obstacle, primary_obstacle_weight, primary_obstacle_lane
	for car = 1, #cars do
		-- dont collision check own car
		if car ~= own_car then
			-- check if car is between next lane switches
			if data.car_between_piece_indices(car, current_index, end_switch_piece) then
			
				--local overtaking_by_lap = false
				--if cars[own_car].piecePosition.lap == 0 then overtaking_by_lap = true end
				--if cars[own_car].piecePosition.lap > cars[car].piecePosition.lap then overtaking_by_lap = true end
			
				local car_is_behind = false
				if cars[car].piecePosition.pieceIndex == cars[own_car].piecePosition.pieceIndex then
					if cars[car].piecePosition.inPieceDistance < cars[own_car].piecePosition.inPieceDistance then
						car_is_behind = true
					end
				end
			
				if not car_is_behind then
					local dist_own_to_switch = data.distance_car_to_piece(own_car, end_switch_piece)
					local dist_obs_to_switch = data.distance_car_to_piece(car, end_switch_piece)
					local error_margin_speed_multiplier = 1.25
					local own_speed = cars[own_car].speed * error_margin_speed_multiplier
					local obs_speed = cars[car].speed + (1/1000000) -- zero speed not valid
					
					-- if my car reaches switch before obstacle reaches switch
					if dist_own_to_switch / own_speed < dist_obs_to_switch / obs_speed then
						this_obstacle_weight = dist_obs_to_switch / obs_speed
						if not primary_obstacle_weight or this_obstacle_weight > primary_obstacle_weight then
							primary_obstacle = car
							primary_obstacle_weight = this_obstacle_weight
							primary_obstacle_lane = cars[car].piecePosition.lane.endLaneIndex
						end
					end
				end
			end
		end
	end
	-- obstacle found
	if primary_obstacle then
		-- if primary obstacle is in the same lane
		if own_lane == primary_obstacle_lane then
			return true
		end
	end
	return false
end

function simulation.turbo_tester(path)
	if data.carPositions[#data.carPositions].own_data.speed == 0 then return false end

	-- count possible turbo powered full throttle ticks, execute if above threshold
	if data.turboAvailable and not data.turboAvailable.start then
		-- dont send throttle message unless last ticks throttle was full
		if data.carPositions[#data.carPositions - 1].throttle == 1.0 then
			-- check that there is some breething room to use turbo
			local current_index = data.carPositions[#data.carPositions].own_data.piecePosition.pieceIndex + 1
			local switch_index = data.next_piece_index(current_index)
			while not data.gameInit.data.race.track.pieces[switch_index].switch do
				switch_index = data.next_piece_index(switch_index)
			end
			
			
			local object_in_way = false
			
			local cars = data.carPositions[#data.carPositions].data
			local own_car = data.own_car_index()
			local own_lane = data.carPositions[#data.carPositions].own_data.piecePosition.lane.endLaneIndex
			for car = 1, #cars do
				if car ~= own_car then
					if own_lane == cars[car].piecePosition.lane.endLaneIndex then
						if data.car_between_piece_indices(car, current_index, switch_index) then
							local car_is_behind = false
							if cars[car].piecePosition.pieceIndex == cars[own_car].piecePosition.pieceIndex then
								if cars[car].piecePosition.inPieceDistance < cars[own_car].piecePosition.inPieceDistance then
									car_is_behind = true
								end
							end
							if not car_is_behind then
								local dist_own_to_switch = data.distance_car_to_piece(own_car, switch_index)
								local dist_obs_to_switch = data.distance_car_to_piece(car, switch_index)
								local error_margin_speed_multiplier = 1.8
								local own_speed = cars[own_car].speed * error_margin_speed_multiplier
								local obs_speed = cars[car].speed + (1/1000000) -- zero speed not valid
								
								-- if my car reaches switch before obstacle reaches switch
								if dist_own_to_switch / own_speed < dist_obs_to_switch / obs_speed then
									object_in_way = true
								end
							end
						end
					end
				end
			end
		
			if not object_in_way then
				if simulation.simulate(simulation.search_depth, 1.0, path, simulation.turbo.throttle_depth_trigger) then
					if simulation.turbo.verification then
						simulation.turbo.verification = nil
						return true
					else
						simulation.turbo.verification = true
					end
				else
					simulation.turbo.verification = nil
				end
			end
		end
	end
	return false
end

--[[
function simulation.binary_search_throttle(depth, switch, fold)
	-- try full throttle first
	if simulation.simulate(depth, 1.0, switch) then
		return 1.0
	-- binary search transition throttle
	else
		local high = 1.0
		local low = 0.0
		local highest_passed_throttle = 0.0
		for i = 1, fold do
			local test_throttle = (low + high) / 2
			if simulation.simulate(depth, test_throttle, switch) then
				low = test_throttle
				highest_passed_throttle = test_throttle
			else
				high = test_throttle
			end
		end
		return highest_passed_throttle
	end
end
]]

function simulation.simulate(depth, throttle, switch, turbo_ticks)
	local angle_limit = simulation.race_parameters.angle_limit
	local maximum_angle_reached = 0
	
	local rr = engine.race_parameters.rr
	local cf = engine.race_parameters.friction
	if not cf then cf = 0.26 end
	
	local hp = engine.race_parameters.hp
	if turbo_ticks or (data.turboAvailable and data.turboAvailable.start) then
		hp = hp * data.turboAvailable.data.turboFactor
		
		-- stricter angle limits for turbo simulation
		angle_limit = simulation.race_parameters.angle_limit - 0.5
	end
	
	local pi = data.carPositions[#data.carPositions].own_data.piecePosition.pieceIndex + 1
	local switch_piece_index
	if simulation.switch.locked_switch_index then
		switch_piece_index = simulation.switch.locked_switch_index
	elseif switch then
		switch_piece_index = data.next_piece_index(pi)
		while not data.gameInit.data.race.track.pieces[switch_piece_index].switch do
			switch_piece_index = data.next_piece_index(switch_piece_index)
		end
	end
	
	local start_lane = data.carPositions[#data.carPositions].own_data.piecePosition.lane.startLaneIndex + 1
	local end_lane = data.carPositions[#data.carPositions].own_data.piecePosition.lane.endLaneIndex + 1
	local switch_done
	if start_lane ~= end_lane then
		switch_done = true
	else
		switch_done = false
	end
	
	local s = data.carPositions[#data.carPositions].own_data.speed
	local a = data.carPositions[#data.carPositions].own_data.angle
	local at = a - data.carPositions[#data.carPositions - 1].own_data.angle
	local pi = data.carPositions[#data.carPositions].own_data.piecePosition.pieceIndex + 1
	local pd = data.carPositions[#data.carPositions].own_data.piecePosition.inPieceDistance
	
	local ite = {}
	ite[0] = {s = s, a = a, at = at, pi = pi, pd = pd}
	
	for i = 1, depth do
		ite[i] = {}
		if i <= (turbo_ticks or 1) then
			ite[i].s = ite[i-1].s + throttle * hp - ite[i-1].s * rr
		else
			ite[i].s = ite[i-1].s + 0.0 * hp - ite[i-1].s * rr
		end

		-- move start and end lanes accordingly
		if switch_piece_index then
			if not switch_done and ite[i-1].pi == switch_piece_index then
				switch_done = true
				if switch == "Left" then
					if start_lane ~= 1 then
						end_lane = start_lane - 1
					end
				elseif switch == "Right" then
					if start_lane ~= #data.gameInit.data.race.track.lanes then
						end_lane = start_lane + 1
					end
				end
			elseif ite[i-1].pi == data.next_piece_index(switch_piece_index) then
				start_lane = end_lane
			end
		end
		
		ite[i].pd = ite[i-1].pd + ite[i].s
		local pl
		if data.gameInit.data.race.track.pieces[ite[i-1].pi].length then
			pl = data.gameInit.data.race.track.pieces[ite[i-1].pi].length
			if switch_piece_index and switch_piece_index == ite[i-1].pi then
				pl = pl + 2.06027499293300
			end
		else
			local pl_start = data.gameInit.data.race.track.pieces[ite[i-1].pi].lengths[start_lane]
			local pl_end = data.gameInit.data.race.track.pieces[ite[i-1].pi].lengths[end_lane]
			-- poor estimation for curved switch piece length
			--pl = (pl_start + pl_end) / 2
			pl = pl_end
		end
				
		ite[i].pi = ite[i-1].pi
		if ite[i].pd > pl then
			ite[i].pd = ite[i].pd - pl
			ite[i].pi = data.next_piece_index(ite[i].pi)
		end		

		local radius = 0
		if data.gameInit.data.race.track.pieces[ite[i-1].pi].angle then
			-- if lane switch is ongoing
			if switch_piece_index == ite[i-1].pi then
				local start_radius = data.gameInit.data.race.track.pieces[ite[i-1].pi].true_radiuses[start_lane]
				local end_radius = data.gameInit.data.race.track.pieces[ite[i-1].pi].true_radiuses[end_lane]
				radius = math.min(start_radius, end_radius)
				radius = radius * 0.75 -- modifier to counter extra curve tightness caused by switching
			else
				radius = data.gameInit.data.race.track.pieces[ite[i-1].pi].true_radiuses[end_lane]
			end
		end

		local sign = 0
		if data.gameInit.data.race.track.pieces[ite[i-1].pi].angle then
			sign = data.sign(data.gameInit.data.race.track.pieces[ite[i-1].pi].angle)
		end

		local so = ite[i-1].s - math.sqrt(radius * cf)
		if so < 0 then so = 0 end
	
		local sr = 0
		if radius ~= 0 then sr = ite[i-1].s / math.sqrt(radius * cf) end
		
		local att = sign * 0.3 * so * sr - 0.1 * ite[i-1].at - 0.00125 * ite[i-1].s * ite[i-1].a
		ite[i].at = ite[i-1].at + att
		ite[i].a = ite[i-1].a + ite[i].at

		-- unkown behaviour with lane switching so stricter angle limits for future iterations
		if switch_done then
			angle_limit = simulation.race_parameters.angle_limit - 4.0
		end
		
		-- keep record of the highest reached angle
		if math.abs(ite[i].a) > maximum_angle_reached then maximum_angle_reached = math.abs(ite[i].a) end
		
		if math.abs(ite[i].a) >= angle_limit then
			return false, maximum_angle_reached
		end
	end
	return true, maximum_angle_reached
end

return simulation