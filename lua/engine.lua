local engine = {}

engine.race_parameters = {}
engine.friction_verification_buffer = {}

function engine.determine_constants()
	if not engine.race_parameters.hp then
		engine.race_parameters.hp = engine.solve_hp()
	end
	
	if not engine.race_parameters.rr then
		engine.race_parameters.rr = engine.solve_rr()
	end
	
	if not engine.race_parameters.friction then
		engine.race_parameters.friction = engine.solve_friction()
	end
end

function engine.solve_friction()
	local friction = nil

	-- needs to be at least tick 2
	if #data.carPositions < 2 then return end
	
	-- slip angle needs to be changing
	if data.carPositions[#data.carPositions].own_data.angle ~= data.carPositions[#data.carPositions - 1].own_data.angle then
		
		-- car needs to be in a curve
		local pi = data.carPositions[#data.carPositions - 1].own_data.piecePosition.pieceIndex + 1
		local lane = data.carPositions[#data.carPositions - 1].own_data.piecePosition.lane.startLaneIndex + 1
		if data.gameInit.data.race.track.pieces[pi].angle then
		
			local radius = data.gameInit.data.race.track.pieces[pi].true_radiuses[lane]
			local s = data.carPositions[#data.carPositions - 1].own_data.speed
			local a = data.carPositions[#data.carPositions - 1].own_data.angle
			local a_now = data.carPositions[#data.carPositions].own_data.angle
			local at = a - data.carPositions[#data.carPositions - 2].own_data.angle
			local sign = data.sign(data.gameInit.data.race.track.pieces[pi].angle)
			
			friction = math.abs((57600 * math.pow(s, 4)) / (radius * math.pow(a * s - 800 * a - 720 * at + 240 * sign * s + 800 * a_now, 2)))
			engine.friction_verification_buffer[#engine.friction_verification_buffer + 1] = friction

			-- full verification buffer
			if #engine.friction_verification_buffer == 2 then
				local sigma = 1 / 1000000
				if math.abs(engine.friction_verification_buffer[1] - engine.friction_verification_buffer[2]) < sigma then
					logger.write(string.format("friction: %.16f", friction))
					return friction
				end
				engine.friction_verification_buffer = {}
			end
		end
	end
end

function engine.solve_hp()
	local hp = nil

	-- needs to be at least tick 2
	if #data.carPositions < 2 then return end
	
	-- throttle needs to be non-zero
	local throttle = data.carPositions[#data.carPositions - 1].throttle
	if throttle ~= 0 then
		local current_speed = data.carPositions[#data.carPositions].own_data.speed
		local previous_speed = data.carPositions[#data.carPositions - 1].own_data.speed
		-- car needs to be on move
		if current_speed ~= 0 then
			local rr = engine.race_parameters.rr
			
			-- if rolling resistance is known
			if rr then
				hp = (previous_speed * (rr - 1) + current_speed) / throttle
				
			-- or car needs to start from standstill
			elseif previous_speed == 0 then
				hp = current_speed / throttle
			end
		end
	end
	if hp and not engine.race_parameters.hp then
		logger.write(string.format("hp: %.16f", hp))
	end
	return hp
end

function engine.solve_rr()
	local rr = nil

	-- needs to be at least tick 2
	if #data.carPositions < 2 then return end
	
	-- car needs to be moving for 2 ticks
	local current_speed = data.carPositions[#data.carPositions].own_data.speed
	local previous_speed = data.carPositions[#data.carPositions - 1].own_data.speed
	if current_speed ~= 0 and previous_speed ~= 0 then
		local throttle = data.carPositions[#data.carPositions - 1].throttle
		local hp = engine.race_parameters.hp
		
		-- throttle needs to be zero
		if throttle == 0 then
			rr = (previous_speed - current_speed ) / previous_speed
			
		-- or hp has to be known
		elseif hp then
			rr = (throttle * hp + previous_speed - current_speed ) / previous_speed
		else
		
		end
	end
	if rr and not engine.race_parameters.rr then
		logger.write(string.format("rr: %.16f", rr))
	end
	return rr
end

return engine